Root shell on machine 1:
`https://<firstname>-<lastname>-machine1.jonctions.com`

To install terraform, instructions are here: https://developer.hashicorp.com/terraform/downloads
To install gcloud CLI, instructions are here: https://cloud.google.com/sdk/docs/install?hl=en#deb

To install the gcloud additional component for terraform validation, run `apt-get install -y google-cloud-cli-terraform-validator`