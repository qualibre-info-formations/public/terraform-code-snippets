variable "project" { default = "itg-sbx-k-moutia" }
variable "region" { default = "us-central1" }
variable "zone" { default = "us-central1-c" }
variable "cidr" { default = "10.0.0.0/16" }