terraform {
    backend "gcs" {
        bucket = "tf-state-<prenom>-<nom>"
        prefix = "terraform/state"
    }
    required_providers {
        google = {
            source = "google"
            version = "~> 4.81"
        }
    }
}